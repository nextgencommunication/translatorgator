def get_installed_apps(kubernetes_image):
    if kubernetes_image == "learn":
        return [
            'core.apps.CoreConfig',
            'learn.apps.LearnConfig',
        ]
    elif kubernetes_image == "admin":
        return [
            'core.apps.CoreConfig',
            'administration.apps.AdministrationConfig',
        ]
    elif kubernetes_image == "jobs":
        return [
            'core.apps.CoreConfig',
        ]
    else:
        return [
            'core.apps.CoreConfig',
            'learn.apps.LearnConfig',
            'administration.apps.AdministrationConfig',
        ]
