from django.conf import settings
from django.urls import include, path
from django.contrib import admin

def get_urlpatterns():
    if settings.KUBERNETES_IMAGE == "learn":
        return [
            path('', include('learn.urls')),
        ]
    elif settings.KUBERNETES_IMAGE == "admin":
        return [
            path('', admin.site.urls),
        ]
    elif settings.KUBERNETES_IMAGE == "jobs":
        return []
    else:
        return [
            path('admin/', admin.site.urls),
            path('learn/', include('learn.urls')),
        ]
