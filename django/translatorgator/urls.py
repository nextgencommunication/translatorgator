from .kubernetes.get_urlpatterns import get_urlpatterns

urlpatterns = get_urlpatterns()
