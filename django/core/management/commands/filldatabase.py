from os import write
from django.core.management.base import BaseCommand
from core.models import Vocab
from core.choices import COLUMNS

import csv


class Command(BaseCommand):
    help = "fills database"
    
    def handle(self, *args, **options):
        print('delete old vocabs')
        Vocab.objects.all().delete()
        print('generate Hiragana vocabs')
        self.open_table('Hiragana_EUC-JP', 'h')
        print('generate Katakana vocabs')
        self.open_table('Katakana_EUC-JP', 'k')
        print('done')

    def open_table(self, file_name, kana):
        with open(f'core/reference_data/{file_name}.csv', 'r', encoding='euc-jp') as file:
            table = csv.reader(file, delimiter=';')
            self.write_table(table, kana)
    
    @staticmethod
    def write_table(table, kana):
        for row in table:
            row_label = row[0].strip(' ')
            
            if 'Einzelgraph' in row_label:
                continue
            
            row_label = row_label.replace('?', '-')
            
            for index, item in enumerate(row[1:]):
                column_label = COLUMNS[index][0]
                
                if not item:
                    continue
                
                item = item.split(',')
                syllable = item[0].strip(' ')
                roman = item[1].strip(' ')
                
                vocab = Vocab(
                    kana=kana,
                    row=row_label,
                    column=column_label,
                    syllable=syllable,
                    roman=roman
                )
                vocab.save()
                
                print(syllable, end=' ')
        print()
