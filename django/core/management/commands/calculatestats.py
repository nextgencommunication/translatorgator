from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from core.models import Stats


class Command(BaseCommand):
    help = "fills database"
    
    def handle(self, *args, **options):
        users = User.objects.all()
        for user in users:
            stats, created = Stats.objects.get_or_create(user=user)
            
            results_count = stats.results.all().count()
            correct_results_count = stats.results.filter(correct=True).count()
            if results_count >= 1:
                average = correct_results_count / results_count
            else:
                average = 0
            
            if stats.average != average:
                stats.average = average
                stats.save()
