KANA = [
    ('h', 'Hiragana'),
    ('k', 'Katakana'),
]

ROWS = [
    ('-','vowels'),
    ('k','k'),
    ('s','s'),
    ('t','t'),
    ('n','n'),
    ('h','h'),
    ('m','m'),
    ('y','y'),
    ('r','r'),
    ('w','w'),
    ('*','*'),
    ('g','g'),
    ('z','z'),
    ('d','d'),
    ('b','b'),
    ('p','p'),
]

COLUMNS = [
    ('a','a'),
    ('i','i'),
    ('u','u'),
    ('e','e'),
    ('o','o'),
    ('ya','ya'),
    ('yu','yu'),
    ('yo','yo'),
]

QUESTION_COUNT = [
    5,
    10,
    15,
    20
]
