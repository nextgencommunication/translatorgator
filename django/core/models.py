from django.db import models
from django.contrib.auth.models import User

from .choices import KANA, ROWS, COLUMNS


class Vocab(models.Model):
    kana = models.CharField(max_length=1, choices=KANA)
    row = models.CharField(max_length=1, choices=ROWS)
    column = models.CharField(max_length=2, choices=COLUMNS)
    syllable = models.CharField(max_length=50)
    roman = models.CharField(max_length=50)


class Stats(models.Model):
    user = models.OneToOneField(User, related_name='stats', on_delete=models.CASCADE)
    average = models.FloatField(default=0)


class Results(models.Model):
    vocab = models.ForeignKey(Vocab, related_name='results', on_delete=models.CASCADE)
    stats = models.ForeignKey(Stats, related_name='results',on_delete=models.CASCADE)
    correct = models.BooleanField()
