import random
from core.models import Vocab

class Quiz():

    def __init__(self, kana, question_count):
        self.kana = kana
        self.question_count = question_count

    def get_random_set(self):
        vocabs = list(Vocab.objects.all().filter(kana=self.kana[0]))
        random.shuffle(vocabs)
        vocabs = vocabs[:self.question_count]
        return vocabs
