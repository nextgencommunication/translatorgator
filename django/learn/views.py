from django.shortcuts import redirect, render
from core.choices import KANA, QUESTION_COUNT
from learn.quiz import Quiz
from learn.forms import QuizForm
from core.models import Vocab


def home(request):
    context = {}

    context["title"] = "Home"
    context["kana_choices"] = KANA
    context["question_count_choices"] = QUESTION_COUNT

    return render(
        request,
        "home.html",
        context
        )

def quiz(request):
    context = {}
    context["title"] = "Quiz"

    if request.method == "POST":
        match(request.POST.get("site_key")):
            case ("home"):
                kana = request.POST.get("kana").split(",")
                question_count = int(request.POST.get("question_count"))
                quiz = Quiz(kana, question_count)
                
                quiz_set = quiz.get_random_set()
                question_number = 0
                wrong_answers = 0

            case ("quiz"):
                quiz_form = QuizForm(request.POST)
                if quiz_form.is_valid():
                    quiz_set = quiz_form.get_quiz_set()
                    vocab = quiz_form.get_vocab()
                    question_number = quiz_form.get_question_number()
                    question_count = quiz_form.get_question_count()
                    wrong_answers = quiz_form.get_wrong_answers()
                    
                    roman = quiz_form.get_roman()

                    if vocab.roman == roman:
                        context["message"] = "Your answer was right!"
                    else:
                        context["message"] = f"Your answer was wrong... Syllable = {vocab.syllable}, Rōmaji = {vocab.roman}. Your answer = {roman}"
                        wrong_answers += 1
                        
                    if not len(quiz_set):
                        context["title"] = "Quiz Results"
                        context["question_count"] = question_count
                        context["wrong_answers"] = wrong_answers
                        context["right_answers"] = question_count - wrong_answers

                        return render(
                            request,
                            "quiz.html",
                            context
                        )
                else:
                    return redirect('home')

        vocab = quiz_set.pop(0)
        question_number += 1
        
        quiz_form = QuizForm()
        quiz_form.set_quiz_set(quiz_set)
        quiz_form.set_vocab(vocab)
        quiz_form.set_question_number(question_number)
        quiz_form.set_question_count(question_count)
        quiz_form.set_wrong_answers(wrong_answers)

        context["vocab"] = vocab
        context["question_number"] = question_number
        context["question_count"] = question_count
        context["wrong_answers"] = wrong_answers
        context["quiz_form"] = quiz_form

        return render(
            request,
            "quiz.html",
            context
        )
            
    else:
        return redirect(home)
