from django import forms
from core.models import Vocab

class QuizForm(forms.Form):
    quiz_set = forms.CharField(widget=forms.HiddenInput(), required=False)
    vocab = forms.CharField(widget=forms.HiddenInput(), required=False)
    question_number = forms.CharField(widget=forms.HiddenInput(), required=False)
    question_count = forms.CharField(widget=forms.HiddenInput(), required=False)
    wrong_answers = forms.CharField(widget=forms.HiddenInput(), required=False)
    roman = forms.CharField(label="",widget=forms.TextInput(attrs={'autofocus': True}),max_length=5)
    
    def set_vocab(self, vocab):
        self.initial["vocab"] = vocab.id

    def get_vocab(self):
        return Vocab.objects.get(id=self.cleaned_data["vocab"])

    def set_quiz_set(self, quiz_set):
        ids_list = [str(vocab.id) for vocab in quiz_set]
        self.initial["quiz_set"] = ",".join(ids_list)

    def get_quiz_set(self):
        ids_string = self.cleaned_data["quiz_set"]
        if ids_string:
            ids_list = self.cleaned_data["quiz_set"].split(",")
            return list(Vocab.objects.filter(id__in=ids_list))
        else:
            return []

    def set_question_number(self, question_number):
        self.initial["question_number"] = question_number

    def get_question_number(self):
        return int(self.cleaned_data["question_number"])

    def set_question_count(self, question_count):
        self.initial["question_count"] = question_count

    def get_question_count(self):
        return int(self.cleaned_data["question_count"])

    def get_roman(self):
        return self.cleaned_data["roman"]

    def set_wrong_answers(self, wrong_answers):
        self.initial["wrong_answers"] = wrong_answers

    def get_wrong_answers(self):
        return int(self.cleaned_data["wrong_answers"])
