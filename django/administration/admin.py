from django.contrib import admin
from core.models import Vocab

@admin.register(Vocab)
class VocabAdmin(admin.ModelAdmin):
    list_display = ('syllable', 'roman', 'kana')
    list_filter = ('row', 'column', 'kana')
