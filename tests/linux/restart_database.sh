DELETE_CMDS = ("kubectl delete -f kubernetes-config/postgres/service.yaml -n tg",
                "kubectl delete -f kubernetes-config/postgres/deployment.yaml -n tg",
                "kubectl delete -f kubernetes-config/postgres/storage.yaml -n tg")
APPLY_CMDS = ("kubectl apply -f kubernetes-config/postgres/storage.yaml -n tg",
                "kubectl apply -f kubernetes-config/postgres/deployment.yaml -n tg",
                "kubectl apply -f kubernetes-config/postgres/service.yaml -n tg")
ROLLOUT_STATUS_CMD = "kubectl rollout status -f kubernetes-config/postgres/deployment.yaml -n tg"

for command in $DELETE_CMDS
do
	eval $command
done

for command in $APPLY_CMDS
do
	eval $command
done

eval $ROLLOUT_STATUS_CMD

URL = "http://translatorgator.germanywestcentral.cloudapp.azure.com" 
content = $(curl -L $URL)

if [![ $content == *'<option value="h,Hiragana">Hiragana</option>'* ]]
then
    sh ./test_utils/message.sh "ALERT" "Restart Database is not RUNNING (restart_database)"
fi
