DELETE_CMD = "kubectl delete -f kubernetes-config\learn\deployment.yaml -n tg"
APPLY_CMD = "kubectl apply -f kubernetes-config\learn\deployment.yaml -n tg"
ROLLOUT_STATUS_CMD = "kubectl rollout status -f kubernetes-config\learn\deployment.yaml -n tg"

eval $DELETE_CMD

eval $APPLY_CMD
eval $ROLLOUT_STATUS_CMD

if [ !$? ]
then
    sh ./test_utils/message.sh "ALERT" "Restart Deployment is not RUNNING (restart_deployment)"
fi
