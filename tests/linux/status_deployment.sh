#!/bin/sh

ROLLOUT_STATUS_CMD = "kubectl rollout status -f kubernetes-config\learn\deployment.yaml -n tg"

eval $ROLLOUT_STATUS_CMD

if [ !$? ]
then
    sh ./test_utils/message.sh "ALERT" "Deployment Status is not RUNNING (status_deployment)"
fi
