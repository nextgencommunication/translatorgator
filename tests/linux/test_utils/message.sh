# specify the pushbullet api key(s)
pushbulletApiKeys = ("o.rOnqysHoEi68wvdp7RkN0cgXDRqXscNm")
alert = $0
message = $1

for apiKey in pushbulletApiKeys
do
	curl --location --request POST 'https://api.pushbullet.com/v2/pushes' \
    --header 'Access-Token: $apiKey' \
    --header 'Content-Type: application/json' \
    --data-raw '{"body":"$message","title":"$alert","type":"note"}'
done
