$DELETE_CMDS = @("kubectl delete -f kubernetes-config/postgres/service.yaml -n tg",
                    "kubectl delete -f kubernetes-config/postgres/deployment.yaml -n tg",
                    "kubectl delete -f kubernetes-config/postgres/storage.yaml -n tg")
$APPLY_CMDS = @("kubectl apply -f kubernetes-config/postgres/storage.yaml -n tg",
                    "kubectl apply -f kubernetes-config/postgres/deployment.yaml -n tg",
                    "kubectl apply -f kubernetes-config/postgres/service.yaml -n tg")
$ROLLOUT_STATUS_CMD = "kubectl rollout status -f kubernetes-config/postgres/deployment.yaml -n tg";

for ($i = 0; $i -lt $DELETE_CMDS.length; $i++){
    Invoke-Expression $DELETE_CMDS[$i]  
}

for ($i = 0; $i -lt $APPLY_CMDS.length; $i++){
    Invoke-Expression $APPLY_CMDS[$i]  
}

Invoke-Expression $ROLLOUT_STATUS_CMD

$WebRequest = Invoke-WebRequest "http://translatorgator.germanywestcentral.cloudapp.azure.com" 
$WebRequestResult = $($WebRequest -match '<option value="h,Hiragana">Hiragana</option>')

if(-not $WebRequestResult) {
    . "$($PSScriptRoot)\test_utils\message.ps1" "ALERT" "Restart Database is not RUNNING (restart_database)"
}
