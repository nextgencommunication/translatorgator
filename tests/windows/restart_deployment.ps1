$DELETE_CMD = "kubectl delete -f kubernetes-config\learn\deployment.yaml -n tg";
$APPLY_CMD = "kubectl apply -f kubernetes-config\learn\deployment.yaml -n tg";
$ROLLOUT_STATUS_CMD = "kubectl rollout status -f kubernetes-config\learn\deployment.yaml -n tg";

Invoke-Expression $DELETE_CMD

Invoke-Expression $APPLY_CMD
Invoke-Expression $ROLLOUT_STATUS_CMD

if(-not $?) {
    . "$($PSScriptRoot)\test_utils\message.ps1" "ALERT" "Restart Deployment is not RUNNING (restart_deployment)"
}
