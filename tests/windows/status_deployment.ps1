$ROLLOUT_STATUS_CMD = "kubectl rollout status -f kubernetes-config\learn\deployment.yaml -n tg";

Invoke-Expression $ROLLOUT_STATUS_CMD

if(-not $?) {
    . "$($PSScriptRoot)\test_utils\message.ps1" "ALERT" "Deployment Status is not RUNNING (status_deployment)"
}