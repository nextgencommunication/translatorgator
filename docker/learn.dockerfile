FROM python:3

WORKDIR /usr/src/app

ENV KUBERNETES_IMAGE learn
ENV PYTHONUNBUFFERED 1

RUN apt-get update -y && \
    apt-get install -y build-essential python3-dev \
    libldap2-dev libsasl2-dev ldap-utils tox \
    lcov valgrind

RUN pip install --upgrade pip
RUN pip install pipenv

# copy every Pipfile and apply config
COPY Pipfile* ./
RUN pipenv install --system --deploy --ignore-pipfile

COPY django .

RUN python manage.py collectstatic

CMD ["uwsgi", "--http-socket", ":8082", "--py-autoreload", "1", "--static-map", "/static=static/", "--module", "translatorgator.wsgi"]
