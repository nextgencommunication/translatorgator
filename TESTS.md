# Tests

## Availability

### 1: Deployment Status
- check deployment status of the main application
- result: Success -> deployment is running

### 2: Container Reset
- kill a pod while running
- result: New pod created -> failsafe

## Scalability

### 3: Pod Replica Creation
- send thousands of requests via JMeter-script.
- result: Pod replicas created -> scaling works

## Persistent Storage

### 4: Persistent Database Storage
- kill the database pods
- result: New pods created with filled database -> persistent storage works

## Monitoring

### 5: Monitoring Software k9s
- k9s in start folder
- show cluster status live on monitoring dashboard
- resources can be evaluated for optimized future resource management
- result: watch cluster state (expected to be OK)
