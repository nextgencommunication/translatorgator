# postgres
kubectl apply -f kubernetes-config/postgres/storage.yaml
kubectl apply -f kubernetes-config/postgres/secrets.yaml
kubectl apply -f kubernetes-config/postgres/deployment.yaml
kubectl apply -f kubernetes-config/postgres/service.yaml

# auth
kubectl apply -f kubernetes-config/auth/storage.yaml
kubectl apply -f kubernetes-config/auth/secrets.yaml
kubectl apply -f kubernetes-config/auth/deployment.yaml
kubectl apply -f kubernetes-config/auth/service.yaml

# jobs
kubectl apply -f kubernetes-config/jobs/filldatabase.yaml
kubectl apply -f kubernetes-config/jobs/migrate.yaml

# learn
kubectl apply -f kubernetes-config/learn/service.yaml
kubectl apply -f kubernetes-config/learn/deployment.yaml
kubectl apply -f kubernetes-config/learn/autoscaler.yaml

# admin
kubectl apply -f kubernetes-config/admin/service.yaml
kubectl apply -f kubernetes-config/admin/deployment.yaml

# ingress
kubectl apply -f kubernetes-config/ingress/ingress.yaml
