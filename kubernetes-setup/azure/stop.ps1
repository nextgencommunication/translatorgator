# ingress
kubectl delete -f kubernetes-config/ingress/ingress.yaml

# learn
kubectl delete -f kubernetes-config/learn/autoscaler.yaml
kubectl delete -f kubernetes-config/learn/service.yaml
kubectl delete -f kubernetes-config/learn/deployment.yaml

# admin
kubectl delete -f kubernetes-config/admin/service.yaml
kubectl delete -f kubernetes-config/admin/deployment.yaml

# jobs
kubectl delete -f kubernetes-config/jobs/filldatabase.yaml
kubectl delete -f kubernetes-config/jobs/migrate.yaml

# auth
kubectl delete -f kubernetes-config/auth/service.yaml
kubectl delete -f kubernetes-config/auth/deployment.yaml
kubectl delete -f kubernetes-config/auth/secrets.yaml
kubectl delete -f kubernetes-config/auth/storage.yaml

# postgres
kubectl delete -f kubernetes-config/postgres/service.yaml
kubectl delete -f kubernetes-config/postgres/deployment.yaml
kubectl delete -f kubernetes-config/postgres/secrets.yaml
kubectl delete -f kubernetes-config/postgres/storage.yaml
