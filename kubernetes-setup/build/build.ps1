# translatorgator-learn
docker build -t registry.gitlab.com/nextgencommunication/translatorgator/learn:latest -f docker/learn.dockerfile .
docker push registry.gitlab.com/nextgencommunication/translatorgator/learn:latest

# translatorgator-jobs
docker build -t registry.gitlab.com/nextgencommunication/translatorgator/jobs:latest -f docker/jobs.dockerfile .
docker push registry.gitlab.com/nextgencommunication/translatorgator/jobs:latest

# translatorgator-admin
docker build -t registry.gitlab.com/nextgencommunication/translatorgator/admin:latest -f docker/admin.dockerfile .
docker push registry.gitlab.com/nextgencommunication/translatorgator/admin:latest
