# translatorgator-learn
echo "build learn image"
docker build -t registry.gitlab.com/nextgencommunication/translatorgator/learn:latest -f docker/learn.dockerfile .
docker push registry.gitlab.com/nextgencommunication/translatorgator/learn:latest

# translatorgator-jobs
echo "build jobs image"
docker build -t registry.gitlab.com/nextgencommunication/translatorgator/jobs:latest -f docker/jobs.dockerfile .
docker push registry.gitlab.com/nextgencommunication/translatorgator/jobs:latest

# translatorgator-admin
echo "build admin image"
docker build -t registry.gitlab.com/nextgencommunication/translatorgator/admin:latest -f docker/admin.dockerfile .
docker push registry.gitlab.com/nextgencommunication/translatorgator/admin:latest
