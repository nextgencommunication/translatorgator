# setup
minikube start
& minikube -p minikube docker-env | Invoke-Expression

$BuildScript = $PSScriptRoot + "\..\build\build.ps1"
& $BuildScript

# postgres
kubectl apply -f kubernetes-config/postgres/storage.yaml
kubectl apply -f kubernetes-config/postgres/secrets.yaml
kubectl apply -f kubernetes-config/postgres/deployment.yaml
kubectl apply -f kubernetes-config/postgres/service.yaml

# jobs
kubectl apply -f kubernetes-config/jobs/filldatabase.yaml
kubectl apply -f kubernetes-config/jobs/migrate.yaml

# learn
kubectl apply -f kubernetes-config/learn/service.yaml
kubectl apply -f kubernetes-config/learn/deployment.yaml

# admin
kubectl apply -f kubernetes-config/admin/service.yaml
kubectl apply -f kubernetes-config/admin/deployment.yaml
