# learn
kubectl delete -f kubernetes-config/learn/service.yaml
kubectl delete -f kubernetes-config/learn/deployment.yaml

# admin
kubectl delete -f kubernetes-config/admin/service.yaml
kubectl delete -f kubernetes-config/admin/deployment.yaml

# jobs
kubectl delete -f kubernetes-config/jobs/filldatabase.yaml
kubectl delete -f kubernetes-config/jobs/migrate.yaml

# postgres
kubectl delete -f kubernetes-config/postgres/service.yaml
kubectl delete -f kubernetes-config/postgres/deployment.yaml
kubectl delete -f kubernetes-config/postgres/storage.yaml
kubectl delete -f kubernetes-config/postgres/secrets.yaml

# stop
minikube stop
